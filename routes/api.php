<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('api')->group(function() {

    Route::get('questions/{amount?}', 'QuestionController@index');

    Route::group(['prefix' => 'question'], function() {
        Route::post('', 'QuestionController@store');
        Route::put('{question}/pick', 'QuestionController@pickAnswer');
        Route::get('{question}', 'QuestionController@show');
        Route::put('{question}', 'QuestionController@update')->middleware('auth');
    });

    // Authentication
    Route::prefix('auth')->group(function() {
        Route::get('current', 'AuthController@current');

        Route::post('login', 'AuthController@authenticate');

        Route::get('logout', 'AuthController@logout');

        Route::get('check', 'AuthController@check');
    });
});



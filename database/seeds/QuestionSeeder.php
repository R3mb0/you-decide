<?php

use App\Question;
use Illuminate\Database\Seeder;

class QuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Question::create([
           'question' => 'Hättest du lieber einen toaster als einzigen freund oder einen starken biber als erzfeind?',
            'answer_1' => 'Taster',
            'answer_2' => 'Biber'
        ]);

        Question::create([
            'question' => 'Wärst du lieber die Krawatte von Gauland oder der rote Blazer von Merkel?',
            'answer_1' => 'Gauland',
            'answer_2' => 'Merkel'
        ]);

        Question::create([
            'question' => 'Hättest du lieber Blümchensex zur Musik von Fler oder Sex mit Fler zur Musik von Blümchen? ',
            'answer_1' => 'Blümchensex',
            'answer_2' => 'Sex mit Fler'
        ]);
    }
}

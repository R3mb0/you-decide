<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function check() {
        return response()->json([
            'authenticated' => Auth::check()
        ]);
    }

    public function current() {
        if(Auth::check()) {
            return new UserResource(Auth::user());
        } else {
            return response()->json([
                'message' => '[Error] No user authenticated',
                'type' => 'no_user_authenticated'
            ]);
        }
    }

    public function register(Request $request) {

        $requestdata = $request->validate([
            'name' => 'required|string|max:255',

            // password_confirmation required
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed'
        ]);

        $user = new User;
        $user->password = Hash::make($requestdata['password']);
        $user->email = $requestdata['email'];
        $user->name = $requestdata['name'];
        $user->group = 'manager';
        $user->save();

        return $this->login($request->email, $request->password);
    }

    public function authenticate(Request $request) {

        $requestdata = $request->validate([
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8']
        ]);

        return $this->login($requestdata['email'], $requestdata['password']);
    }

    public function logout() {
        if(Auth::check()) {
            Auth::logout();
        } else {
            abort(402, [
                "message" => "Logout failed"
            ]);
        }
    }

    public function login($email, $password) {

        $credentials = [
            'email' => $email,
            'password' => $password
        ];

        if (Auth::attempt($credentials)) {
            return new UserResource(Auth::user());
        } else {
            return response()->json(
                [
                    'message' => 'Login failed',
                    'errors' =>
                        [
                            "email" => ["Email oder Passwort falsch."]
                        ]
                ], 402);
        }
    }
}

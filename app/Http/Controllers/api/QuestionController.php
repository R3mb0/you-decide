<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreQuestionRequest;
use App\Http\Resources\QuestionCollection;
use App\Http\Resources\QuestionResource;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return QuestionCollection
     */
    public function index()
    {
        $amount = Route::current()->parameter('amount');

        if($amount != null) {
            return new QuestionCollection(Question::paginate($amount));
        }

        return new QuestionCollection(Question::all());
    }

    public function store(StoreQuestionRequest $request)
    {
        $question = new Question;

        $question->question = $request->question;
        $question->answer_1 = $request->answer_1;
        $question->answer_2 = $request->answer_2;

        $question->save();
        return new QuestionResource($question);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function pickAnswer(Question $question, Request $request) {

        if($request->answer == 1)
            $question->answer_1_clicked += 1;

        if($request->answer == 2)
            $question->answer_2_clicked += 1;

        $question->save();
        return new QuestionResource($question);
    }
}

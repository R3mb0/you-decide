require('./bootstrap');

import Vue from 'Vue';
import App from './App.vue'

import Axios from 'axios';

import router from "./router";
import VueRouter from 'vue-router';

import Transitions from 'vue2-transitions';

Axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

let token = document.head.querySelector('meta[name="csrf-token"]');

if (token) {
    window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
} else {
    console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}

Vue.prototype.$http = Axios;

Vue.use(VueRouter);
Vue.use(Transitions);

const vm = new Vue({
    router,
    render: h => h(App),
}).$mount('#app');

import Router from 'vue-router';

import home from './views/Home.vue';
import submit from './views/Submit.vue';
import validate from './views/Validate.vue';

const routes = [
    { name: 'home', path: '/', component: home },
    { name: 'submit', path: '/submit', component: submit },
    { name: 'validate', path: '/validate', component: validate }
];

const router = new Router({
    routes,
    mode: 'hash'
});

export default router;
